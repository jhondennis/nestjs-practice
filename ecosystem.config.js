module.exports = {
  apps: [
    {
      name: 'BIBLIO_API',
      script: 'yarn start:prod',
      watch: false
    }
  ]
};
