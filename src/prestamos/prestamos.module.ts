import { Module } from '@nestjs/common';
import { PrestamosService } from './prestamos.service';
import { PrestamosController } from './prestamos.controller';
import { LibrosModule } from 'src/libros/libros.module';

@Module({
  controllers: [PrestamosController],
  providers: [PrestamosService],
  imports: [LibrosModule]
})
export class PrestamosModule {}
