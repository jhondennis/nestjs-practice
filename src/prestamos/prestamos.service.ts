import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
// import { Cron } from '@nestjs/schedule';

import { PrismaService } from 'src/prisma/prisma.service';
import { CreatePrestamoDto } from './dto/create-prestamo.dto';
import { UpdatePrestamoDto } from './dto/update-prestamo.dto';
import { LibrosService } from 'src/libros/libros.service';

@Injectable()
export class PrestamosService {
  constructor(
    private readonly prisma: PrismaService,
    private libro: LibrosService
  ) {}
  private readonly select: Prisma.PrestamoSelect = {
    id: true,
    devuelto: true,
    prestado: true,
    dias_prestamo: true,
    estado: true,
    libro: {
      select: {
        id: true,
        anio: true,
        autor: true,
        estado: true,
        titulo: true,
        edicion: true,
        sig_tip: true,
        editorial: true,
        url_imagen: true,
        procedencia: true,
        descripcion: true,
        url_descarga: true,
        category: true,
        numero_inventario: true,
        createdAt: true,
        updatedAt: true,
        disponible: true
      }
    },
    libro_id: true,
    usuario: {
      select: {
        id: true,
        RU: true,
        apellidos: true,
        carrera: true,
        celular: true,
        ci: true,
        email: true,
        estado: true,
        nombres: true,
        rol: { select: { id: true, nombre: true } },
        rol_id: true
      }
    },
    usuario_id: true,
    createdAt: true,
    updatedAt: true,
    usuario_devolucion: {
      select: {
        id: true,
        RU: true,
        apellidos: true,
        carrera: true,
        celular: true,
        ci: true,
        email: true,
        estado: true,
        nombres: true,
        rol: { select: { id: true, nombre: true } },
        rol_id: true
      }
    },
    usuario_devolucion_id: true,
    usuario_prestamo: {
      select: {
        id: true,
        RU: true,
        apellidos: true,
        carrera: true,
        celular: true,
        ci: true,
        email: true,
        estado: true,
        nombres: true,
        rol: { select: { id: true, nombre: true } },
        rol_id: true
      }
    },
    usuario_prestamo_id: true
  };

  create(createPrestamoDto: CreatePrestamoDto) {
    return new Promise((resolve, reject) => {
      this.prisma.prestamo
        .create({
          data: createPrestamoDto as Prisma.PrestamoUncheckedCreateInput,
          select: this.select
        })
        .then(async (response: any) => {
          await this.libro
            .update(response.libro_id, {
              ...response.libro,
              estado: 'PRESTADO'
            })
            .then((resp) => (response.libro = resp))
            .catch((e) => console.log('Error al actualizar el libro', e));

          resolve(response);
        })
        .catch((e) =>
          reject({
            message: `Error al crear prestamo: ${e.meta?.target || ''}`,
            ...e
          })
        );
    });
  }

  create_reservation(createPrestamoDto: CreatePrestamoDto) {
    return new Promise((resolve, reject) => {
      this.prisma.prestamo
        .create({
          data: createPrestamoDto as Prisma.PrestamoUncheckedCreateInput,
          select: this.select
        })
        .then(async (response: any) => {
          await this.libro
            .update(response.libro_id, {
              ...response.libro,
              estado: 'RESERVADO'
            })
            .then((resp) => (response.libro = resp))
            .catch(() => console.log('Error al actualizar el libro'));

          resolve(response);
        })
        .catch((e) =>
          reject({
            message: `Error al crear la reserva: ${e.meta?.target || ''}`,
            ...e
          })
        );
    });
  }

  findAll() {
    return new Promise((resolve, reject) => {
      this.prisma.prestamo
        .findMany({ select: this.select, orderBy: { createdAt: 'desc' } })
        .then((response) => resolve(response))
        .catch((e) => reject(e));
    });
  }

  findOne(id: string) {
    return new Promise((resolve, reject) => {
      this.prisma.prestamo
        .findUnique({
          where: { id: id } as Prisma.PrestamoWhereUniqueInput,
          select: this.select
        })
        .then((response) => resolve(response))
        .catch((e) => reject({ message: 'Error al buscar el prestamo', ...e }));
    });
  }

  update(id: string, updatePrestamoDto: UpdatePrestamoDto) {
    return new Promise((resolve, reject) => {
      this.prisma.prestamo
        .update({
          where: { id: id } as Prisma.PrestamoWhereUniqueInput,
          data: updatePrestamoDto as Prisma.PrestamoUncheckedUpdateInput,
          select: this.select
        })
        .then((response) => resolve(response))
        .catch((e) =>
          reject({
            message: `Error al actualizar el prestamo: ${e.meta?.target || ''}`,
            ...e
          })
        );
    });
  }

  remove(id: string) {
    return new Promise((resolve, reject) => {
      this.prisma.prestamo
        .delete({
          where: { id: id } as Prisma.PrestamoWhereUniqueInput,
          select: this.select
        })
        .then((response) => resolve(response))
        .catch((e) =>
          reject({ message: 'Error al eliminar el prestamo', code: e.code })
        );
    });
  }

  finalize(id: string, estado: string, updatePrestamoDto: UpdatePrestamoDto) {
    return new Promise((resolve, reject) => {
      this.prisma.prestamo
        .update({
          where: { id: id } as Prisma.PrestamoWhereUniqueInput,
          data: updatePrestamoDto as Prisma.PrestamoUncheckedUpdateInput,
          select: this.select
        })
        .then(async (response: any) => {
          await this.libro
            .update(response.libro_id, {
              ...response.libro,
              estado: estado
            })
            .then((resp) => (response.libro = resp))
            .catch((e) => console.log('Error al actualizar el libro', e));

          resolve(response);
        })
        .catch((e) =>
          reject({
            message: `Error al actualizar el prestamo: ${e.meta?.target || ''}`,
            ...e
          })
        );
    });
  }

  findPrestamo(id: string) {
    return new Promise((resolve, reject) => {
      this.prisma.prestamo
        .findMany({
          where: { usuario: { id: id } } as Prisma.PrestamoWhereUniqueInput,
          orderBy: { createdAt: 'desc' },
          select: {
            id: true,
            devuelto: true,
            prestado: true,
            dias_prestamo: true,
            estado: true,
            libro: {
              select: {
                id: true,
                anio: true,
                autor: true,
                estado: true,
                titulo: true,
                edicion: true,
                sig_tip: true,
                editorial: true,
                url_imagen: true,
                procedencia: true,
                descripcion: true,
                url_descarga: true,
                numero_inventario: true,
                category: true,
                createdAt: true,
                updatedAt: true,
                disponible: true
              }
            },
            libro_id: true,
            usuario_id: true,
            createdAt: true,
            updatedAt: true
          }
        })
        .then((response) => resolve(response))
        .catch((e) =>
          reject({
            message: `Error: ${e.meta?.target || ''}`,
            ...e
          })
        );
    });
  }

  reportFindBook(from: string, to: string, state: any, quantity: number) {
    return new Promise((resolve, reject) => {
      this.prisma.prestamo
        .groupBy({
          by: ['libro_id', 'estado'],
          where: {
            estado: state,
            updatedAt: { gte: from },
            AND: { updatedAt: { lte: to } }
          },
          _count: {
            libro_id: true
          },
          orderBy: {
            _count: { libro_id: 'desc' }
          },
          take: Number(quantity)
        })
        .then(async (response) => {
          const result = [];
          for (const item of response) {
            const resp = await this.prisma.libro.findFirst({
              where: { id: item.libro_id }
            });

            if (resp) result.push({ ...item, libro: resp });
            else result.push(item);
          }

          resolve(result);
        })
        .catch((e) =>
          reject({
            message: `Error: ${e.meta?.target || ''}`,
            ...e
          })
        );
    });
  }

  findAllReport(from: string, to: string, state: any) {
    let where = {};
    if (state !== '')
      where = {
        estado: state,
        updatedAt: { gte: from },
        AND: { updatedAt: { lte: to } }
      };
    else
      where = {
        updatedAt: { gte: from },
        AND: { updatedAt: { lte: to } }
      };
    return new Promise((resolve, reject) => {
      this.prisma.prestamo
        .findMany({
          where: where,
          select: this.select
        })
        .then((response) => {
          resolve(response);
        })
        .catch((e) =>
          reject({
            message: `Error: ${e.meta?.target || ''}`,
            ...e
          })
        );
    });
  }

  // @Cron('45 * * * * *')
  // clean_reservation() {
  //   this.prisma.prestamo
  //     .findMany({
  //       where: { libro: { estado: 'RESERVADO' } },
  //       select: this.select
  //     })
  //     .then((prestamos: []) => {
  //       // console.log('-->', new Date(), prestamos);
  //     })
  //     .catch((e) => console.log(e));
  // }
}
