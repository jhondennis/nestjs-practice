import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  HttpStatus,
  Res,
  Query
} from '@nestjs/common';
import { PrestamosService } from './prestamos.service';
import { CreatePrestamoDto } from './dto/create-prestamo.dto';
import { UpdatePrestamoDto } from './dto/update-prestamo.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('Prestamos')
@Controller({ version: '1', path: 'prestamos' })
export class PrestamosController {
  constructor(private readonly prestamosService: PrestamosService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiOperation({ summary: 'Registrar prestamo' })
  create(@Body() createPrestamoDto: CreatePrestamoDto, @Res() res: any) {
    return this.prestamosService
      .create(createPrestamoDto)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Post('reserva')
  @HttpCode(HttpStatus.CREATED)
  @ApiOperation({ summary: 'Registrar reserva' })
  createReservation(
    @Body() createPrestamoDto: CreatePrestamoDto,
    @Res() res: any
  ) {
    return this.prestamosService
      .create_reservation(createPrestamoDto)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Listar prestamos' })
  findAll(@Res() res: any) {
    return this.prestamosService
      .findAll()
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Get('reporte')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({
    summary:
      'Reporte de Prestamos intervalo entre fechas, por estado, agrupado por libro'
  })
  findReportBook(
    @Query('from') from: string,
    @Query('to') to: string,
    @Query('state') state: string,
    @Query('quantity') quantity: number,
    @Res() res: any
  ) {
    return this.prestamosService
      .reportFindBook(from, to, state, quantity)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Get('reporte-prestamos')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({
    summary: 'Reporte de prestamos entre fechas y por estado.'
  })
  findReportPrestamo(
    @Query('from') from: string,
    @Query('to') to: string,
    @Query('state') state: string,
    @Res() res: any
  ) {
    return this.prestamosService
      .findAllReport(from, to, state)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Obtener prestamo por id' })
  findOne(@Param('id') id: string, @Res() res: any) {
    return this.prestamosService
      .findOne(id)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Actualizar prestamo' })
  update(
    @Param('id') id: string,
    @Body() updatePrestamoDto: UpdatePrestamoDto,
    @Res() res: any
  ) {
    return this.prestamosService
      .update(id, updatePrestamoDto)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Patch('estado/:id/:estado')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Finalizar prestamo ID prestamo, ESTADO del libro' })
  finalize(
    @Param('id') id: string,
    @Param('estado') estado: string,
    @Body() updatePrestamoDto: UpdatePrestamoDto,
    @Res() res: any
  ) {
    return this.prestamosService
      .finalize(id, estado, updatePrestamoDto)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Delete(':id')
  remove(@Param('id') id: string, @Res() res: any) {
    return this.prestamosService
      .remove(id)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Get('usuario/:id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Obtener los prestamos de un USUARIO por ID' })
  findPrestamosByUserID(@Param('id') id: string, @Res() res: any) {
    return this.prestamosService
      .findPrestamo(id)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }
}
