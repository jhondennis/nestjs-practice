import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsDateString, IsOptional, IsString } from 'class-validator';
import { CreatePrestamoDto } from './create-prestamo.dto';

export class UpdatePrestamoDto extends PartialType(CreatePrestamoDto) {
  @ApiProperty()
  @IsString({ message: 'DIAS PRESTAMO Tiene que ser un string' })
  @IsOptional()
  dias_prestamo: string;

  @ApiProperty({
    default: 'RESERVADO',
    description: 'Estado del prestamo',
    enum: [
      'PRESTADO',
      'RESERVADO',
      'DEVUELTO',
      'DISPONIBLE',
      'NO_DISPONIBLE',
      'CANCELADO'
    ]
  })
  @IsString({ message: 'ESTADO Tiene que ser un string' })
  @IsOptional()
  estado: string;

  @ApiProperty({
    default: '2022-01-22T23:22:04.428Z',
    description: 'Tiene que ser un dato tipo Date IsoString'
  })
  @IsDateString({
    message: 'DEVUELTO Tiene que ser un dato tipo Date IsoString'
  })
  @IsOptional()
  devuelto: Date;

  @ApiProperty({
    default: '2022-01-22T23:22:04.428Z',
    description: 'Tiene que ser un dato tipo Date IsoString'
  })
  @IsDateString({
    message: 'PRESTADO Tiene que ser un dato tipo Date IsoString'
  })
  @IsOptional()
  prestado: Date;

  @ApiProperty()
  @IsString({ message: 'LIBRO ID Tiene que ser un string' })
  @IsOptional()
  libro_id: string;

  @ApiProperty()
  @IsString({ message: 'USUARIO ID Tiene que ser un string' })
  @IsOptional()
  usuario_id: string;

  @ApiProperty()
  @IsString({ message: 'USUARIO PRESTAMO ID Tiene que ser un string' })
  @IsOptional()
  usuario_prestamo_id: string;

  @ApiProperty()
  @IsString({ message: 'USUARIO DEVOLUCION ID Tiene que ser un string' })
  @IsOptional()
  usuario_devolucion_id: string;
}
