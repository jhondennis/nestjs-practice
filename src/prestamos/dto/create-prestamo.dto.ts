import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsOptional, IsString } from 'class-validator';

export class CreatePrestamoDto {
  @ApiProperty({
    default: '3',
    description: 'Cantidad de dias para el prestamo'
  })
  @IsString({ message: 'DIAS PRESTAMO Tiene que ser un string' })
  dias_prestamo: string;

  @ApiProperty({
    default: 'RESERVADO',
    description: 'Estado del prestamo',
    enum: [
      'PRESTADO',
      'RESERVADO',
      'DEVUELTO',
      'DISPONIBLE',
      'NO_DISPONIBLE',
      'CANCELADO'
    ]
  })
  @IsString({ message: 'ESTADO Tiene que ser un string' })
  estado: string;

  @ApiProperty({
    default: '2022-01-22T23:22:04.428Z',
    description: 'Tiene que ser un dato tipo Date IsoString'
  })
  @IsDateString({
    message: 'DEVUELTO Tiene que ser un dato tipo Date IsoString'
  })
  @IsOptional()
  devuelto: Date;

  @ApiProperty({
    default: '2022-01-22T23:22:04.428Z',
    description: 'Tiene que ser un dato tipo Date IsoString'
  })
  @IsDateString({
    message: 'PRESTADO Tiene que ser un dato tipo Date IsoString'
  })
  @IsOptional()
  prestado: Date;

  @ApiProperty()
  @IsString({ message: 'LIBRO ID Tiene que ser un string' })
  libro_id: string;

  @ApiProperty()
  @IsString({ message: 'USUARIO ID Tiene que ser un string' })
  usuario_id: string;

  @ApiProperty()
  @IsOptional()
  @IsString({ message: 'USUARIO PRESTAMO ID Tiene que ser un string' })
  usuario_prestamo_id: string;
}
