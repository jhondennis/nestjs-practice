import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { AppController } from './app.controller';
import { AppService } from './app.service';

import { PrismaModule } from './prisma/prisma.module';
import { UsuariosModule } from './usuarios/usuarios.module';
import { RolModule } from './rol/rol.module';
import { LibrosModule } from './libros/libros.module';
import { AuthModule } from './auth/auth.module';
import { PrestamosModule } from './prestamos/prestamos.module';

@Module({
  imports: [
    PrismaModule,
    UsuariosModule,
    RolModule,
    LibrosModule,
    AuthModule,
    PrestamosModule,
    ScheduleModule.forRoot()
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
