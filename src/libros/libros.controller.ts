import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Res,
  HttpStatus,
  HttpCode
} from '@nestjs/common';
import { LibrosService } from './libros.service';
import { CreateLibroDto } from './dto/create-libro.dto';
import { UpdateLibroDto } from './dto/update-libro.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('Libros')
@Controller({ version: '1', path: 'books' })
export class LibrosController {
  constructor(private readonly librosService: LibrosService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiOperation({ summary: 'Registrar nuevo libro' })
  create(@Body() createLibroDto: CreateLibroDto, @Res() res: any) {
    this.librosService
      .create(createLibroDto)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Listar todos los libros' })
  findAll(@Res() res: any) {
    this.librosService
      .findAll()
      .then((response) => {
        res.send(response);
      })
      .catch((e) => {
        res.status(HttpStatus.BAD_REQUEST).send(e);
      });
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Obtener un libro por id' })
  findOne(@Param('id') id: string, @Res() res: any) {
    this.librosService
      .findOne(id)
      .then((response) => {
        res.send(response);
      })
      .catch((e) => {
        res.status(HttpStatus.BAD_REQUEST).send(e);
      });
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Actualizar libro' })
  update(
    @Param('id') id: string,
    @Body() updateLibroDto: UpdateLibroDto,
    @Res() res: any
  ) {
    this.librosService
      .update(id, updateLibroDto)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Eliminar libro' })
  remove(@Param('id') id: string, @Res() res: any) {
    this.librosService
      .remove(id)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }
}
