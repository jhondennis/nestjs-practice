import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';

import { PrismaService } from 'src/prisma/prisma.service';
import { CreateLibroDto } from './dto/create-libro.dto';
import { UpdateLibroDto } from './dto/update-libro.dto';

@Injectable()
export class LibrosService {
  constructor(private readonly prisma: PrismaService) {}
  private readonly select: Prisma.LibroSelect = {
    id: true,
    anio: true,
    autor: true,
    estado: true,
    titulo: true,
    edicion: true,
    sig_tip: true,
    editorial: true,
    url_imagen: true,
    procedencia: true,
    descripcion: true,
    url_descarga: true,
    category: true,
    numero_inventario: true,
    createdAt: true,
    updatedAt: true,
    disponible: true
  };

  create(createLibroDto: CreateLibroDto) {
    return new Promise((resolve, reject) => {
      this.prisma.libro
        .create({
          data: createLibroDto as Prisma.LibroCreateInput,
          select: this.select
        })
        .then((response) => {
          resolve(response);
        })
        .catch((e) => {
          reject({
            message: `Error al crear el libro: ${e.meta?.target || ''}`,
            ...e
          });
        });
    });
  }

  findAll() {
    return new Promise((resolve, reject) => {
      this.prisma.libro
        .findMany({
          select: this.select,
          orderBy: { createdAt: 'desc' }
        })
        .then((response) => {
          resolve(response);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  findOne(id: string) {
    return new Promise((resolve, reject) => {
      this.prisma.libro
        .findUnique({
          where: { id: id },
          select: this.select
        })
        .then((response) => {
          resolve(response);
        })
        .catch((e) => {
          reject({ message: 'Error al buscar el libro', e });
        });
    });
  }

  update(id: string, updateLibroDto: UpdateLibroDto): any {
    return new Promise((resolve, reject) => {
      this.prisma.libro
        .update({
          where: { id: id },
          data: updateLibroDto as Prisma.LibroUncheckedUpdateInput,
          select: this.select
        })
        .then((response) => resolve(response))
        .catch((e) =>
          reject({
            message: `Error al actualizar el libro: ${e.meta?.target || ''}`,
            ...e
          })
        );
    });
  }

  remove(id: string) {
    return new Promise((resolve, reject) => {
      this.prisma.libro
        .delete({
          where: { id: id },
          select: this.select
        })
        .then((response) => {
          resolve(response);
        })
        .catch((e) => {
          reject({ message: 'Error al eliminar el libro', code: e.code });
        });
    });
  }
}
