import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsDateString, IsOptional, IsString } from 'class-validator';

export class CreateLibroDto {
  @ApiProperty()
  @IsString({ message: 'TITULO Tiene que ser un string' })
  titulo: string;

  @ApiProperty()
  @IsString({ message: '#INVENTARIO Tiene que ser un string' })
  numero_inventario: string;

  @ApiProperty()
  @IsString({ message: 'SIG_TIP Tiene que ser un string' })
  sig_tip: string;

  @ApiProperty()
  @IsString({ message: 'DESCRIPCION Tiene que ser un string' })
  @IsOptional()
  descripcion: string;

  @ApiProperty()
  @IsString({ message: 'AUTOR Tiene que ser un string' })
  @IsOptional()
  autor: string;

  @ApiProperty({
    default: '2022-01-22T23:22:04.428Z',
    description: 'Tiene que ser un dato tipo Date IsoString'
  })
  @IsDateString({
    message: 'AÑO Tiene que ser un dato tipo Date IsoString'
  })
  @IsOptional()
  anio: Date;

  @ApiProperty()
  @IsString({ message: 'EDICION Tiene que ser un string' })
  @IsOptional()
  edicion: string;

  @ApiProperty()
  @IsString({ message: 'PROCEDENCIA Tiene que ser un string' })
  @IsOptional()
  procedencia: string;

  @ApiProperty()
  @IsString({ message: 'EDITORIAL Tiene que ser un string' })
  @IsOptional()
  editorial: string;

  @ApiProperty()
  @IsString({ message: 'URL-DESCARGA Tiene que ser un string' })
  @IsOptional()
  url_descarga: string;

  @ApiProperty()
  @IsString({ message: 'URL-IMAGEN Tiene que ser un string' })
  @IsOptional()
  url_imagen: string;

  @ApiProperty()
  @IsString({ message: 'Category Tiene que ser un string' })
  @IsOptional()
  category: string;

  @ApiProperty({
    default: 'DISPONIBLE',
    description: 'Estado del libro',
    enum: [
      'PRESTADO',
      'RESERVADO',
      'DEVUELTO',
      'DISPONIBLE',
      'NO_DISPONIBLE',
      'CANCELADO'
    ]
  })
  @IsString({ message: 'ESTADO Tiene que ser un string' })
  estado: string;

  @ApiProperty()
  @IsBoolean({ message: 'DISPONIBLE Tiene que ser falso o verdadero' })
  disponible: boolean;
}
