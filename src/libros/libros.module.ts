import { Module } from '@nestjs/common';
import { LibrosService } from './libros.service';
import { LibrosController } from './libros.controller';
import { PrismaService } from 'src/prisma/prisma.service';

@Module({
  controllers: [LibrosController],
  providers: [LibrosService, PrismaService],
  exports: [LibrosService]
})
export class LibrosModule {}
