import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';

@Injectable()
export class UsuariosService {
  constructor(private readonly prisma: PrismaService) {}

  private readonly select = {
    id: true,
    RU: true,
    apellidos: true,
    carrera: true,
    celular: true,
    ci: true,
    email: true,
    estado: true,
    nombres: true,
    rol: { select: { id: true, nombre: true } },
    rol_id: true,
    createdAt: true,
    updatedAt: true
  } as Prisma.UsuarioSelect;

  create(createUsuarioDto: CreateUsuarioDto) {
    return new Promise((resolve, reject) => {
      this.prisma.usuario
        .create({
          data: createUsuarioDto as Prisma.UsuarioUncheckedCreateInput,
          select: this.select
        })
        .then((response) => {
          resolve(response);
        })
        .catch((e) => {
          reject({
            message: `Error al crear el usuario: ${e.meta?.target || ''}`,
            ...e
          });
        });
    });
  }

  findAll() {
    return new Promise((resolve, reject) => {
      this.prisma.usuario
        .findMany({
          select: this.select
        })
        .then((response) => {
          resolve(response);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  findOne(id: string) {
    return new Promise((resolve, reject) => {
      this.prisma.usuario
        .findUnique({
          where: { id: id } as Prisma.UsuarioWhereUniqueInput,
          select: this.select
        })
        .then((response) => {
          resolve(response);
        })
        .catch((e) => {
          reject({ message: 'Error al buscar el usuario', ...e });
        });
    });
  }

  update(id: string, updateUsuarioDto: UpdateUsuarioDto) {
    return new Promise((resolve, reject) => {
      this.prisma.usuario
        .update({
          where: { id: id } as Prisma.UsuarioWhereUniqueInput,
          data: updateUsuarioDto as Prisma.UsuarioUncheckedUpdateInput,
          select: this.select
        })
        .then((response) => resolve(response))
        .catch((e) =>
          reject({
            message: `Error al actualizar el usuario: ${e.meta?.target || ''}`,
            ...e
          })
        );
    });
  }

  remove(id: string) {
    return new Promise((resolve, reject) => {
      this.prisma.usuario
        .delete({
          where: { id: id } as Prisma.UsuarioWhereUniqueInput
        })
        .then((response) => resolve(response))
        .catch((e) =>
          reject({ message: 'Error al eliminar el usuario', ...e })
        );
    });
  }
}
