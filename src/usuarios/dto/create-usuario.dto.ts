import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString } from 'class-validator';

export class CreateUsuarioDto {
  @ApiProperty({
    default: 'correo@gmail.com',
    description: 'Correo electronico'
  })
  @IsEmail({}, { message: 'Email tiene que ser un correo valido' })
  email: string;

  @ApiProperty({
    default: '123456',
    description: 'Cedula de identidad'
  })
  @IsString({ message: 'CI Tiene que ser un string' })
  ci: string;

  @ApiProperty({
    default: '123456',
    description: 'Registro unico de la Universidad'
  })
  @IsString({ message: 'RU Tiene que ser un string' })
  RU: string;

  @ApiProperty({
    default: 'Usuario ' + Math.floor(Math.random() * 10),
    description: 'Nombre del usuario'
  })
  @IsString({ message: 'NOMBRES Tiene que ser un string' })
  nombres: string;

  @ApiProperty({
    default: 'Apellido ' + Math.floor(Math.random() * 10),
    description: 'Apellidos del usuario'
  })
  @IsString({ message: 'APELLIDOS Tiene que ser un string' })
  apellidos: string;

  @ApiProperty({
    default: 'ACTIVO',
    description: 'Estado activo o bloquedo del usuario'
  })
  @IsString({ message: 'ESTADO Tiene que ser un string' })
  estado: string;

  @ApiProperty({
    default: '123456789',
    description: 'Celular del usuario'
  })
  @IsString({ message: 'CELULAR Tiene que ser un string' })
  celular: string;

  @ApiProperty({
    default: 'Ing. Sistemas',
    description: 'Carrera del usuario'
  })
  @IsString({ message: 'CARRERA Tiene que ser un string' })
  carrera: string;

  @ApiProperty()
  @IsString({ message: 'ROL_ID Tiene que ser un string' })
  rol_id: string;
}
