import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
  HttpCode
} from '@nestjs/common';
import { UsuariosService } from './usuarios.service';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('Usuarios') // SEPARA POR TAGS EN EL SWAGER
@Controller({ version: '1', path: 'user' }) // añade la version y el path en el endpoint
export class UsuariosController {
  constructor(private readonly usuariosService: UsuariosService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiOperation({ summary: 'Registrar usuario' })
  create(@Body() createUsuarioDto: CreateUsuarioDto, @Res() res: any) {
    this.usuariosService
      .create(createUsuarioDto)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Listar usuarios' })
  findAll(@Res() res: any) {
    this.usuariosService
      .findAll()
      .then((response) => res.send(response))
      .catch((e) => {
        res.status(HttpStatus.BAD_REQUEST).send(e);
      });
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Obtener un usuario por id' })
  findOne(@Param('id') id: string, @Res() res: any) {
    this.usuariosService
      .findOne(id)
      .then((response) => {
        res.send(response);
      })
      .catch((e) => {
        res.status(HttpStatus.BAD_REQUEST).send(e);
      });
  }

  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Actualizar usuario' })
  update(
    @Param('id') id: string,
    @Body() updateUsuarioDto: UpdateUsuarioDto,
    @Res() res: any
  ) {
    this.usuariosService
      .update(id, updateUsuarioDto)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Eliminar usuario' })
  remove(@Param('id') id: string, @Res() res: any) {
    this.usuariosService
      .remove(id)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }
}
