import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
  HttpCode
} from '@nestjs/common';
import { RolService } from './rol.service';
import { CreateRolDto } from './dto/create-rol.dto';
import { UpdateRolDto } from './dto/update-rol.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('Roles')
@Controller({ version: '1', path: 'rol' })
export class RolController {
  constructor(private readonly rolService: RolService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiOperation({ summary: 'Registrar rol' })
  create(@Body() createRolDto: CreateRolDto, @Res() res: any) {
    return this.rolService
      .create(createRolDto)
      .then((response) => {
        res.send(response);
      })
      .catch((e) => {
        res.status(HttpStatus.BAD_REQUEST).send(e);
      });
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Listar roles' })
  findAll(@Res() res: any) {
    return this.rolService
      .findAll()
      .then((response) => {
        res.send(response);
      })
      .catch((e) => {
        res.status(HttpStatus.BAD_REQUEST).send(e);
      });
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Obtener un rol por id' })
  findOne(@Param('id') id: string, @Res() res: any) {
    return this.rolService
      .findOne(id)
      .then((response) => {
        res.send(response);
      })
      .catch((e) => {
        res.status(HttpStatus.BAD_REQUEST).send(e);
      });
  }

  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Actualizar Rol' })
  update(
    @Param('id') id: string,
    @Body() updateRolDto: UpdateRolDto,
    @Res() res: any
  ) {
    this.rolService
      .update(id, updateRolDto)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }

  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Eliminar Rol' })
  remove(@Param('id') id: string, @Res() res: any) {
    this.rolService
      .remove(id)
      .then((response) => res.send(response))
      .catch((e) => res.status(HttpStatus.BAD_REQUEST).send(e));
  }
}
