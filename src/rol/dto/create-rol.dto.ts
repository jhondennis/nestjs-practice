import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateRolDto {
  @ApiProperty({ default: 'ADMIN', description: 'Nombre del rol' })
  @IsString({ message: 'Tiene que ser un string' })
  nombre: string;
}
