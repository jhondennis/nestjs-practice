import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateRolDto } from './dto/create-rol.dto';
import { UpdateRolDto } from './dto/update-rol.dto';

@Injectable()
export class RolService {
  constructor(private readonly prisma: PrismaService) {}

  create(createRolDto: CreateRolDto) {
    return new Promise((resolve, reject) => {
      this.prisma.rol
        .create({
          data: createRolDto as Prisma.RolCreateInput,
          select: { id: true, nombre: true }
        })
        .then((response) => {
          resolve(response);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  findAll() {
    return new Promise((resolve, reject) => {
      this.prisma.rol
        .findMany({
          select: {
            id: true,
            nombre: true
          }
        })
        .then((response) => {
          resolve(response);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  findOne(id: string) {
    return new Promise((resolve, reject) => {
      this.prisma.rol
        .findUnique({
          where: {
            id: id
          } as Prisma.RolWhereUniqueInput
        })
        .then((response) => {
          resolve(response);
        })
        .catch((e) => {
          reject({ message: 'Error al buscar el rol', e });
        });
    });
  }

  update(id: string, updateRolDto: UpdateRolDto) {
    return new Promise((resolve, reject) => {
      this.prisma.rol
        .update({
          where: { id: id } as Prisma.RolWhereUniqueInput,
          data: updateRolDto as Prisma.RolUncheckedUpdateInput
        })
        .then((response) => resolve(response))
        .catch((e) => reject({ message: 'Error al actualizar el rol', ...e }));
    });
  }

  remove(id: string) {
    return new Promise((resolve, reject) => {
      this.prisma.rol
        .delete({
          where: { id: id } as Prisma.RolWhereUniqueInput
        })
        .then((response) => resolve(response))
        .catch((e) => reject({ message: 'Error al eliminar el rol', ...e }));
    });
  }
}
