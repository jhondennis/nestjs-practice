import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateAuthDto } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';

@Injectable()
export class AuthService {
  constructor(private readonly prisma: PrismaService) {}

  create(createAuthDto: CreateAuthDto) {
    return 'This action adds a new auth';
  }

  findAll() {
    return `This action returns all auth`;
  }

  findOne(id: number) {
    return `This action returns a #${id} auth`;
  }

  update(id: number, updateAuthDto: UpdateAuthDto) {
    return `This action updates a #${id} auth`;
  }

  remove(id: number) {
    return `This action removes a #${id} auth`;
  }

  login(login) {
    return new Promise((resolve, reject) => {
      this.prisma.usuario
        .findFirst({
          where: {
            AND: [
              {
                RU: login.ru
              },
              {
                ci: login.ci
              }
            ]
          },
          select: {
            id: true,
            ci: true,
            RU: true,
            apellidos: true,
            nombres: true,
            carrera: true,
            celular: true,
            email: true,
            estado: true,
            rol: { select: { nombre: true } },
            createdAt: true,
            updatedAt: true
          }
        })
        .then((response) => {
          if (response === null)
            reject({ message: 'Usuario o contraseña incorrecto' });
          else {
            resolve({
              ...response,
              rol: response.rol.nombre
            });
          }
        })
        .catch((e) => {
          reject({ message: 'Usuario o contraseña incorrecto', e });
        });
    });
  }
}
