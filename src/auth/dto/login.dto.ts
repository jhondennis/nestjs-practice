import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class LoginDto {
  @ApiProperty({
    description: 'RU',
    default: '123456'
  })
  @IsString({ message: 'Debe ingresar una cadena' })
  ru: string;

  @ApiProperty({
    description: 'C.I.',
    default: '123456'
  })
  ci: string;
}
